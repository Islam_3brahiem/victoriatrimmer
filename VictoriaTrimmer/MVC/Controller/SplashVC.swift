//
//  SplashVC.swift
//  VictoriaTrimmer
//
//  Created by hesham ghalaab on 10/23/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var circleDashView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addCircleDash()
        self.textLabel.textColor = UIColor.white.withAlphaComponent(0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        fadeTextColorIn()
    }
    
    private func fadeTextColorIn(){
        UIView.transition(with: textLabel, duration: 2, options: .transitionCrossDissolve, animations: {
            self.textLabel.textColor = UIColor.white.withAlphaComponent(1)
        }) { (_) in
            self.goToRoot()
        }
    }
    
    private func goToRoot(){
        guard let window = self.view.window else { return }
        
        if Defaults.isUserLogged{
            Route().toTabBarController(window: window)
        }else{
            Route().toLoginController(window: window)
        }
    }

    private func addCircleDash(){
        let radius: CGFloat = 72.0
        let circle = CAShapeLayer()
        let roundedRect = CGRect(x: 0, y: 0, width: 2.0*radius, height: 2.0*radius)
        circle.path = UIBezierPath(roundedRect: roundedRect, cornerRadius: radius).cgPath
        
        // Configure the apperence of the circle
        circle.fillColor = UIColor.clear.cgColor
        circle.strokeColor = UIColor.white.cgColor
        circle.lineWidth = 2;
        circle.lineDashPattern = [2, 4]
        circleDashView.layer.addSublayer(circle)
    }
    
}
