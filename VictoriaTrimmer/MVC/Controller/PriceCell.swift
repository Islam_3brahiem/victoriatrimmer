//
//  PriceCell.swift
//  VictoriaTrimmer
//
//  Created by hesham ghalaab on 10/23/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit

protocol PriceCellDelegate: class {
    func payNowBtnCLicked(_ cell: UICollectionViewCell)
}

class PriceCell: UICollectionViewCell {
    
    var delegate: PriceCellDelegate?
    
    @IBOutlet weak var priceBackgroundView: UIView!
    @IBOutlet weak var priceImageView: UIImageView!
    @IBOutlet weak var priceTitleLabel: UILabel!
    @IBOutlet weak var priceSubTitleLabel: UILabel!
    
    @IBOutlet weak var priceDescriptionLabel1: UILabel!
    @IBOutlet weak var priceDescriptionLabel2: UILabel!
    @IBOutlet weak var priceDescriptionLabel3: UILabel!
    @IBOutlet weak var priceDescriptionLabel4: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func onTapPayNow(_ sender: UIButton) {
        delegate?.payNowBtnCLicked(self)
    }
    
}
