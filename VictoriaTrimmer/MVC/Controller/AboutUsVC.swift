//
//  AboutUsVC.swift
//  VictoriaTrimmer
//
//  Created by hesham ghalaab on 10/23/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit

class AboutUsVC: UIViewController {

    @IBOutlet weak var circleDashView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addCircleDash()
        setupNavigation()
    }
    
    @IBAction func onTapLogout(_ sender: UIBarButtonItem) {
        Defaults.isUserLogged = false
        guard let window = self.view.window else { return }
        Route().toLoginController(window: window)
    }
    
    private func setupNavigation(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }

    private func addCircleDash(){
        let radius: CGFloat = 72.0
        let circle = CAShapeLayer()
        let roundedRect = CGRect(x: 0, y: 0, width: 2.0*radius, height: 2.0*radius)
        circle.path = UIBezierPath(roundedRect: roundedRect, cornerRadius: radius).cgPath
        
        // Configure the apperence of the circle
        circle.fillColor = UIColor.clear.cgColor
        circle.strokeColor = UIColor.white.cgColor
        circle.lineWidth = 2;
        circle.lineDashPattern = [2, 4]
        circleDashView.layer.addSublayer(circle)
    }
}
