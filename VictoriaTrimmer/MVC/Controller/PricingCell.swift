//
//  PricingCell.swift
//  VictoriaTrimmer
//
//  Created by hesham ghalaab on 10/23/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit

class PricingCell: UICollectionViewCell {
    @IBOutlet weak var priceBackgroundView: UIView!
    @IBOutlet weak var priceImageView: UIImageView!
//    @IBOutlet weak var priceTitleLabel: UILabel!
//    @IBOutlet weak var priceSubTitleLabel: UILabel!
//    @IBOutlet weak var priceDescriptionLabel: UILabel!
}
