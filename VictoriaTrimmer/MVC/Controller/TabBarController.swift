//
//  TabBarController.swift
//  VictoriaTrimmer
//
//  Created by hesham ghalaab on 10/23/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit
import SSCustomTabbar
extension UIImage{
    //Draws the top indicator by making image with filling color
    class func drawTabBarIndicator(color: UIColor, size: CGSize, onTop: Bool) -> UIImage {
        let indicatorHeight = size.height / 30
        let yPosition = onTop ? 0 : (size.height - indicatorHeight)
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: yPosition, width: size.width, height: indicatorHeight))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
}

class TabBarController: UITabBarController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        clearTitlesForAllVCs()
        setDefaultTitle()
        
        self.delegate = self
        self.tabBar.tintColor = .white
        self.tabBar.unselectedItemTintColor = UIColor().assets(named: .transparent)
    }
}

//https://github.com/msalah1193/TTabBarViewController

extension TabBarController: UITabBarControllerDelegate{

    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        clearTitlesForAllVCs()
        if viewController is HomeVC{
            viewController.tabBarItem.title = "Home"
        }else if viewController is PricingVC{
            viewController.tabBarItem.title = "Pricing"
        }else if viewController is ProfileVC{
            viewController.tabBarItem.title = "Profile"
        }else if viewController is AboutUsVC{
            viewController.tabBarItem.title = "About Us"
        }
    }
    
    func clearTitlesForAllVCs(){
        guard let vcs = self.viewControllers else {return}
        vcs.forEach { (vc) in
            vc.tabBarItem.title = ""
        }
    }
    
    func setDefaultTitle(){
        guard let vcs = self.viewControllers else {return}
        vcs[0].tabBarItem.title = "Home"
    }
}

