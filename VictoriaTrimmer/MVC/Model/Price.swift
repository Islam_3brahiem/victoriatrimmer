//
//  Price.swift
//  VictoriaTrimmer
//
//  Created by hesham ghalaab on 10/23/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit

struct Price {
    let title: String
    let subTitle: String
    let desc1: String
    let desc2: String
    let desc3: String
    let desc4: String
    let image: UIImage
    let color: UIColor
    let price: String
    
    
    static func prices() -> [Price]{
        var prices = [Price]()
        
        let price1 = Price(title: "BLUE BALLOON",
                           subTitle: "100 USD Per Month",
                           desc1: "- You can pick up to 100 video",
                           desc2: "- You can edit up to 100 video",
                           desc3: "- You can share up to 100 video",
                           desc4: "- You can shave up to 100 video",
                           image: UIImage(named: "blue")!,
                           color: #colorLiteral(red: 0.2250417173, green: 0.4750272632, blue: 0.7946702838, alpha: 1),
                           price: "100")
        
        let price2 = Price(title: "Red BALLOON",
                           subTitle: "150 USD Per Month",
                           desc1: "- You can pick up to 150 video",
                           desc2: "- You can edit up to 150 video",
                           desc3: "- You can share up to 150 video",
                           desc4: "- You can shave up to 150 video",
                           image: UIImage(named: "red")!,
                           color: #colorLiteral(red: 0.9431296587, green: 0.3130689561, blue: 0.2045525014, alpha: 1),
                           price: "150")
        
        let price3 = Price(title: "Gold BALLOON",
                           subTitle: "300 USD Per Month",
                           desc1: "- You can pick up to 300 video",
                           desc2: "- You can edit up to 300 video",
                           desc3: "- You can share up to 300 video",
                           desc4: "- You can shave up to 300 video",
                           image: UIImage(named: "gold")!,
                           color: #colorLiteral(red: 1, green: 0.7301751971, blue: 0, alpha: 1),
                           price: "300")
        
        prices.append(price1)
        prices.append(price2)
        prices.append(price3)
        return prices
    }
}
