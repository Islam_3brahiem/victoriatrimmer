//
//  ProfileVC.swift
//  VictoriaTrimmer
//
//  Created by hesham ghalaab on 10/23/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {

    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var emailfield: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigation()
        userNameField.attributedPlaceholder = NSAttributedString(string: "Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        emailfield.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        phoneField.attributedPlaceholder = NSAttributedString(string: "Mobile Number", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        passwordField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let user = Defaults.user
        userNameField.text = user.name
        emailfield.text = user.email
        phoneField.text = user.phone
        passwordField.text = ""
    }
    

    @IBAction func onTapUpdate(_ sender: UIButton) {
        guard let email = emailfield.text , !email.isEmpty else {
            self.alertUser(title: "", message: "please fill all the fields.")
            return
        }
        guard let password = passwordField.text , !password.isEmpty else {
            self.alertUser(title: "", message: "please fill all the fields.")
            return
        }
        
        guard let phone = phoneField.text , !phone.isEmpty else {
            self.alertUser(title: "", message: "please fill all the fields.")
            return
        }
        guard let userName = userNameField.text , !userName.isEmpty else {
            self.alertUser(title: "", message: "please fill all the fields.")
            return
        }
        
        let id = Defaults.user.id
        update(email: email, password: password, name: userName, phone: phone, id: id)
    }
    
    private func update(email: String, password: String, name: String, phone: String, id: Int){
           showLoading()
        Services.update(withEmail: email, passowrd: password, phone: phone, name: name, id: id) { (response) in
               self.hideLoading()
               switch response{
               case .failure(let error):
                   self.alertUser(title: "", message: error.localizedDescription)
               case .success(let data):
                   print(data)
                   self.passwordField.text = ""
                   if let user = data.user{
                        Defaults.user = user
                   }
                   
                   self.alertUser(title: "Successfully updated", message: "")
               }
           }
       }

    @IBAction func onTapLogout(_ sender: UIBarButtonItem) {
        Defaults.isUserLogged = false
        guard let window = self.view.window else { return }
        Route().toLoginController(window: window)
    }
    
    private func setupNavigation(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
}
