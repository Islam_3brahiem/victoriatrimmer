//
//  Services.swift
//  VictoriaTrimmer
//
//  Created by hesham ghalaab on 10/26/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import Foundation
import Alamofire

class Services {
    
    static func baseUrl() -> URL?{
        let urlString = "https://videotrimer.000webhostapp.com/video_trimmer_web_service.php"
        return URL(string: urlString)
    }
    
    typealias loginCompletion = (Result<LoginResponse>) -> ()
    static func login(withEmail email: String, passowrd: String, completion: @escaping loginCompletion){
        guard let url = baseUrl() else {
            completion(.failure(ServicesError.urlError))
            return
        }
        let param = ["method": "login" ,
                     "email": email,
                     "password": passowrd]
        
        var header = [String: String]()
        header["Content-Type"] = "application/json"
        
        Services.printRequestDetails(url: url, method: .post, parameters: param, headers: header)
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header)
            .validate()
            .responseJSON { (response) in
                
                print(response)
                switch response.result{
                case .success(_):
                    guard let _data = response.data else {
                        completion(.failure(ServicesError.unExpected))
                        return
                    }
                    
                    do{
                        let data = try JSONDecoder().decode(LoginResponse.self, from: _data)
                        let stateResult = Services.handle(state: data.state)
                        
                        switch stateResult {
                        case .failure(let error):
                            completion(.failure(error))
                        case .success(_):
                            completion(.success(data))
                        }
                        
                    } catch{
                        print("----------------")
                        print(error)
                        completion(.failure(ServicesError.decodeingError))
                    }
                    
                case .failure(let error):
                    let responseError = error.localizedDescription
                    print("😡 Failure \(responseError)")
                    completion(.failure(error))
                }
        }
    }
    
    typealias signupCompletion = (Result<SignUpResponse>) -> ()
    static func signup(withEmail email: String, passowrd: String, phone: String, name: String, completion: @escaping signupCompletion){
        guard let url = baseUrl()else {
            completion(.failure(ServicesError.urlError))
            return
        }
        let param = ["method":"register" ,
                     "name": name,
                     "email": email,
                     "password": passowrd ,
                     "phone": phone ]
        
        var header = [String: String]()
        header["Content-Type"] = "application/json"
        
        Services.printRequestDetails(url: url, method: .post, parameters: param, headers: header)
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header)
            .validate()
            .responseJSON { (response) in
                
                print(response)
                switch response.result{
                case .success(_):
                    guard let _data = response.data else {
                        completion(.failure(ServicesError.unExpected))
                        return
                    }
                    
                    do{
                        let data = try JSONDecoder().decode(SignUpResponse.self, from: _data)
                        let stateResult = Services.handle(state: data.state)
                        
                        switch stateResult {
                        case .failure(let error):
                            completion(.failure(error))
                        case .success(_):
                            completion(.success(data))
                        }
                        
                    } catch{
                        print("----------------")
                        print(error)
                        completion(.failure(ServicesError.decodeingError))
                    }
                    
                case .failure(let error):
                    let responseError = error.localizedDescription
                    print("😡 Failure \(responseError)")
                    completion(.failure(error))
                }
        }
    }
    
    typealias updateCompletion = (Result<UpdateResponse>) -> ()
    static func update(withEmail email: String, passowrd: String, phone: String, name: String, id: Int, completion: @escaping updateCompletion){
        guard let url = baseUrl()else {
            completion(.failure(ServicesError.urlError))
            return
        }
        let param = ["method":"update_profile" ,
                     "name": name,
                     "email": email,
                     "password": passowrd ,
                     "phone": phone,
                     "user_id": id] as [String : Any]
        
        var header = [String: String]()
        header["Content-Type"] = "application/json"
        
        Services.printRequestDetails(url: url, method: .post, parameters: param, headers: header)
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header)
            .validate()
            .responseJSON { (response) in
                
                print(response)
                switch response.result{
                case .success(_):
                    guard let _data = response.data else {
                        completion(.failure(ServicesError.unExpected))
                        return
                    }
                    
                    do{
                        let data = try JSONDecoder().decode(UpdateResponse.self, from: _data)
                        let stateResult = Services.handle(state: data.state)
                        
                        switch stateResult {
                        case .failure(let error):
                            completion(.failure(error))
                        case .success(_):
                            completion(.success(data))
                        }
                        
                    } catch{
                        print("----------------")
                        print(error)
                        completion(.failure(ServicesError.decodeingError))
                    }
                    
                case .failure(let error):
                    let responseError = error.localizedDescription
                    print("😡 Failure \(responseError)")
                    completion(.failure(error))
                }
        }
    }
    
    
    /// Codes: 101 >> sucess , 102 >> empty , 103 >> wrong mail , 104 >> failed , 105 >> used
    static func handle(state: Int) -> (Result<Bool>){
        switch state {
        case 101:
            return .success(true)
        case 102:
            return .failure(ServicesError.empty)
        case 103:
            return .failure(ServicesError.wrongMail)
        case 104:
            return .failure(ServicesError.failed)
        case 105:
            return .failure(ServicesError.used)
        default:
            return .failure(ServicesError.failed)
        }
    }
    
    static func printRequestDetails(url: URL, method: HTTPMethod, parameters: [String: Any]?, headers: [String: String]?){
           print("\n***************************** // *****************************")
           print("✊ \(method) With URL: \(url)")
           if let keys = headers?.keys{ print("✊ Headers: \(keys)") } else { print("✊ No Headers")}
           if let para = parameters{ print("✊ Parameters: \(para)") } else { print("✊ No Parameters")}
           print("***************************** // *****************************\n")
       }
}

enum ServicesError: Error {
    case unExpected
    case decodeingError
    case urlError
    
    // states
    case empty
    case wrongMail
    case failed
    case used
}

extension ServicesError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .unExpected:
            return "error occured while handling data, please try again later."
            
        case .decodeingError:
            return "error in decoding, please try again later."
            
        case .urlError:
            return "cannot find this url."
        
        case .empty:
            return "fields are empty."
        
        case .wrongMail:
            return "email is Wrong."
            
        case .failed:
            return "error occured, please try again later."
        case .used:
            return "used data."
        }
    }
}
