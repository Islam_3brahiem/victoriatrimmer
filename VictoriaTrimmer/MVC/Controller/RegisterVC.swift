//
//  RegisterVC.swift
//  VictoriaTrimmer
//
//  Created by hesham ghalaab on 10/23/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController {

    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var emailfield: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var circleDashView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addCircleDash()
        userNameField.attributedPlaceholder = NSAttributedString(string: "Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        emailfield.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        phoneField.attributedPlaceholder = NSAttributedString(string: "Mobile Number", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        passwordField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }
    
    

    func isValidEmail(emailStr:String) -> Bool {
          let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
          let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
          return emailPred.evaluate(with: emailStr)
      }
    
    @IBAction func onTapRegster(_ sender: UIButton) {
        guard let email = emailfield.text , !email.isEmpty else {
            self.alertUser(title: "", message: "please fill all the fields.")
            return
        }
        guard let password = passwordField.text , !password.isEmpty else {
            self.alertUser(title: "", message: "please fill all the fields.")
            return
        }
        
        guard let phone = phoneField.text , !phone.isEmpty else {
            self.alertUser(title: "", message: "please fill all the fields.")
            return
        }
        guard let userName = userNameField.text , !userName.isEmpty else {
            self.alertUser(title: "", message: "please fill all the fields.")
            return
        }
        
        if !isValidEmail(emailStr: email) {
            self.alertUser(title: "", message: "Please insert valid email")
            return
        }
        
        
         if  ( phone.count < 7 )  {
            self.alertUser(title: "", message: "Please insert valid phone")
            return
        }
        
        if  ( password.count < 6 )  {
                   self.alertUser(title: "", message: "Password should be at least 6 characters")
                   return
               }
        
        signup(withEmail: email, password: password, name: userName, phone: phone)
    }
    
    @IBAction func onTapAlreadyHaveAccount(_ sender: UIButton) {
        guard let window = self.view.window else { return }
        Route().toLoginController(window: window)
    }
    
    private func addCircleDash(){
        let radius: CGFloat = 72.0
        let circle = CAShapeLayer()
        let roundedRect = CGRect(x: 0, y: 0, width: 2.0*radius, height: 2.0*radius)
        circle.path = UIBezierPath(roundedRect: roundedRect, cornerRadius: radius).cgPath
        
        // Configure the apperence of the circle
        circle.fillColor = UIColor.clear.cgColor
        circle.strokeColor = UIColor.white.cgColor
        circle.lineWidth = 2;
        circle.lineDashPattern = [2, 4]
        circleDashView.layer.addSublayer(circle)
    }
    
    private func signup(withEmail email: String, password: String, name: String, phone: String){
        showLoading()
        Services.signup(withEmail: email, passowrd: password, phone: phone, name: name) { (response) in
            self.hideLoading()
            switch response{
            case .failure(let error):
                self.alertUser(title: "", message: error.localizedDescription)
            case .success(let data):
                print(data)
                Defaults.isUserLogged = true
                if let user = data.user{
                     Defaults.user = user
                }
                guard let window = self.view.window else { return }
                Route().toTabBarController(window: window)
            }
        }
    }

}
