//
//  Route.swift
//  VictoriaTrimmer
//
//  Created by hesham ghalaab on 10/23/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit

class Route {
    
    func toLoginController(window: UIWindow){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC")
        
        clearPresntedControllers()
        window.backgroundColor = UIColor(hue: 0.6477,
                                         saturation: 0.6314,
                                         brightness: 0.6077,
                                         alpha: 0.8)
        
        UIView.transition(with: window, duration: 0.55001,
                          options: .transitionFlipFromLeft,
                          animations: nil, completion: nil)
        
        window.makeKeyAndVisible()
        window.rootViewController = vc
    }
    
    func toRegisterController(window: UIWindow){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RegisterVC")
        
        clearPresntedControllers()
        window.backgroundColor = UIColor(hue: 0.6477,
                                         saturation: 0.6314,
                                         brightness: 0.6077,
                                         alpha: 0.8)
        
        UIView.transition(with: window, duration: 0.55001,
                          options: .transitionFlipFromLeft,
                          animations: nil, completion: nil)
        
        window.makeKeyAndVisible()
        window.rootViewController = vc
    }
    
    
    
    func toTabBarController(window: UIWindow){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TabBarController")
        
        clearPresntedControllers()
        window.backgroundColor = UIColor(hue: 0.6477,
                                         saturation: 0.6314,
                                         brightness: 0.6077,
                                         alpha: 0.8)
        
        UIView.transition(with: window, duration: 0.55001,
                          options: .transitionFlipFromLeft,
                          animations: nil, completion: nil)
        
        window.makeKeyAndVisible()
        window.rootViewController = vc
    }
    
    private func clearPresntedControllers(){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        guard let window = appDelegate.window else { return }
        
        guard window.rootViewController?.presentedViewController != nil else {return}
        window.rootViewController?.dismiss(animated: false, completion: {
            self.clearPresntedControllers()
        })
    }
}
