//
//  SignUpResponse.swift
//  VictoriaTrimmer
//
//  Created by hesham ghalaab on 10/26/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import Foundation

struct SignUpResponse: Decodable {
    let state: Int
    let user: User?
}
