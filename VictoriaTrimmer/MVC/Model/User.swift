//
//  User.swift
//  VictoriaTrimmer
//
//  Created by hesham ghalaab on 10/26/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import Foundation

struct User: Decodable {
    let id: Int
    let name: String
    let email: String
    let phone: String
    let google_id: String
}
