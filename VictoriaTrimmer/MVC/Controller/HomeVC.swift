//
//  HomeVC.swift
//  VictoriaTrimmer
//
//  Created by hesham ghalaab on 10/23/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit
import Lottie

class HomeVC: UIViewController {

    @IBOutlet weak var baseView: LottieView!
    
    let animationView = AnimationView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addAnimation()
        setupNavigation()
    }
    
    private func addAnimation(){
        let animation = Animation.named("waves")
        animationView.animation = animation
        animationView.loopMode = .loop
        
        animationView.frame.size.width = 375
        animationView.frame.size.height = 375
        animationView.center = self.view.center
        animationView.contentMode = .scaleAspectFill
        
        baseView.addSubview(animationView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animationView.play()
        
    }
    
    @IBAction func onTapLogout(_ sender: UIBarButtonItem) {
        Defaults.isUserLogged = false
        Defaults.removeUser()
        guard let window = self.view.window else { return }
        Route().toLoginController(window: window)
    }
    
    private func setupNavigation(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
}
