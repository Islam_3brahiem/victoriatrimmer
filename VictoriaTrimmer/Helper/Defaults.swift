//
//  Defaults.swift
//  VictoriaTrimmer
//
//  Created by hesham ghalaab on 10/23/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit

class Defaults{
    static var isUserLogged: Bool {
        set{
            UserDefaults.standard.set(newValue, forKey: "isUserLogged")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.bool(forKey: "isUserLogged")
        }
    }
    
    static var user: User {
        set{
            UserDefaults.standard.set(newValue.id, forKey: "id")
            UserDefaults.standard.set(newValue.name, forKey: "name")
            UserDefaults.standard.set(newValue.email, forKey: "email")
            UserDefaults.standard.set(newValue.phone, forKey: "phone")
            UserDefaults.standard.set(newValue.google_id, forKey: "google_id")
            UserDefaults.standard.synchronize()
        }
        get{
            let id = UserDefaults.standard.integer(forKey: "id")
            let name = UserDefaults.standard.string(forKey: "name") ?? ""
            let email = UserDefaults.standard.string(forKey: "email") ?? ""
            let phone = UserDefaults.standard.string(forKey: "phone") ?? ""
            let google_id = UserDefaults.standard.string(forKey: "google_id") ?? ""
            
            return User(id: id, name: name, email: email, phone: phone, google_id: google_id)
        }
    }
    
    static func removeUser(){
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "id")
        defaults.removeObject(forKey: "name")
        defaults.removeObject(forKey: "email")
        defaults.removeObject(forKey: "phone")
        defaults.synchronize()
    }
    
    
}

extension UIViewController{
    func alertUser(title: String, message: String){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: { (action: UIAlertAction!) in
                // Handle Cancel Logic here
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
}
