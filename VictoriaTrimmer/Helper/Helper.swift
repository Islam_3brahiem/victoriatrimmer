//
//  Helper.swift
//  VictoriaTrimmer
//
//  Created by hesham ghalaab on 10/23/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit

extension UIColor{
    func assets(named: TheColors) -> UIColor{
        guard let theColor = UIColor(named: named.rawValue) else { return .black }
        return theColor
    }
}

enum TheColors: String {
    case accent = "Accent"
    case transparent = "Transparent"
    case primary = "Primary"
    case primaryDark = "PrimaryDark"
    case primaryLight = "PrimaryLight"
}

//@available(iOS 13.0, *)
//extension UIResponder {
//    @objc var scene: UIScene? {
//        return nil
//    }
//}
//
//@available(iOS 13.0, *)
//extension UIScene {
//    @objc override var scene: UIScene? {
//        return self
//    }
//}
//
//@available(iOS 13.0, *)
//extension UIView {
//    @objc override var scene: UIScene? {
//        if let window = self.window {
//            return window.windowScene
//        } else {
//            return self.next?.scene
//        }
//    }
//}
//
//@available(iOS 13.0, *)
//extension UIViewController {
//    @objc override var scene: UIScene? {
//        // Try walking the responder chain
//        var res = self.next?.scene
//        if (res == nil) {
//            // That didn't work. Try asking my parent view controller
//            res = self.parent?.scene
//        }
//        if (res == nil) {
//            // That didn't work. Try asking my presenting view controller
//            res = self.presentingViewController?.scene
//        }
//
//        return res
//    }
//}


import UIKit

extension UIView {
    
    func roundView(withCorner radius: CGFloat, borderColor: UIColor = .clear, borderWidth: CGFloat = 0){
        layer.cornerRadius = radius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        layer.masksToBounds = true
    }
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
                layer.masksToBounds = true
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}


public let kShapeDashed : String = "kShapeDashed"

extension UIView {
    
    func removeDashedBorder(_ view: UIView) {
        view.layer.sublayers?.forEach {
            if kShapeDashed == $0.name {
                $0.removeFromSuperlayer()
            }
        }
    }

    func addDashedBorder(width: CGFloat? = nil, height: CGFloat? = nil, lineWidth: CGFloat = 2, lineDashPattern:[NSNumber]? = [6,3], strokeColor: UIColor = UIColor.red, fillColor: UIColor = UIColor.clear) {
        
        
        var fWidth: CGFloat? = width
        var fHeight: CGFloat? = height
        
        if fWidth == nil {
            fWidth = self.frame.width
        }
        
        if fHeight == nil {
            fHeight = self.frame.height
        }
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()

        let shapeRect = CGRect(x: 0, y: 0, width: fWidth!, height: fHeight!)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: fWidth!/2, y: fHeight!/2)
        shapeLayer.fillColor = fillColor.cgColor
        shapeLayer.strokeColor = strokeColor.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = lineDashPattern
        shapeLayer.name = kShapeDashed
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
    
}
