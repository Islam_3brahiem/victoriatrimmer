//
//  LoginVC.swift
//  VictoriaTrimmer
//
//  Created by hesham ghalaab on 10/23/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit
import Alamofire

class LoginVC: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var circleDashView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addCircleDash()
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email Or Phone", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }
    
    @IBAction func onTapLogin(_ sender: UIButton) {
        
        guard let email = emailTextField.text , !email.isEmpty else {
            self.alertUser(title: "", message: "please fill all the fields.")
            return
        }
        guard let password = passwordTextField.text , !password.isEmpty else {
            self.alertUser(title: "", message: "please fill all the fields.")
            return
        }
        
        
        
        login(withEmail: email, passowrd: password)
    }
    
    @IBAction func onTapDonotHaveAccount(_ sender: UIButton) {
        guard let window = self.view.window else { return }
        Route().toRegisterController(window: window)
    }
    
  

    
    private func addCircleDash(){
        let radius: CGFloat = 72.0
        let circle = CAShapeLayer()
        let roundedRect = CGRect(x: 0, y: 0, width: 2.0*radius, height: 2.0*radius)
        circle.path = UIBezierPath(roundedRect: roundedRect, cornerRadius: radius).cgPath
        
        // Configure the apperence of the circle
        circle.fillColor = UIColor.clear.cgColor
        circle.strokeColor = UIColor.white.cgColor
        circle.lineWidth = 2;
        circle.lineDashPattern = [2, 4]
        circleDashView.layer.addSublayer(circle)
    }
    
    private func login(withEmail email: String, passowrd: String){
        showLoading()
        Services.login(withEmail: email, passowrd: passowrd) { (response) in
            self.hideLoading()
            switch response{
            case .failure(let error):
                self.alertUser(title: "", message: error.localizedDescription)
            case .success(let data):
                print(data)
                Defaults.isUserLogged = true
                if let user = data.user{
                     Defaults.user = user
                }
                guard let window = self.view.window else { return }
                Route().toTabBarController(window: window)
            }
        }
    }
}
