//
//  PricingVC.swift
//  VictoriaTrimmer
//
//  Created by hesham ghalaab on 10/23/19.
//  Copyright © 2019 hesham ghalaab. All rights reserved.
//

import UIKit

class PricingVC: UIViewController {

    @IBOutlet weak var priceCollectionView: UICollectionView!
    
    var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    var payPalConfig = PayPalConfiguration()
    
    var prices = [Price]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        configure()
        payPal()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        PayPalMobile.preconnect(withEnvironment: environment)
    }
    
    private func payPal() {
        payPalConfig.acceptCreditCards = false
        payPalConfig.merchantName = "Victoria"//Give your company name here.
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        //This is the language in which your paypal sdk will be shown to users.
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        //Here you can set the shipping address. You can choose either the address associated with PayPal account or different address. We’ll use .both here.
        payPalConfig.payPalShippingAddressOption = .both;

    }
    
    private func configure(){
        prices = Price.prices()
        priceCollectionView.delegate = self
        priceCollectionView.dataSource = self
        
        let priceCellNib = UINib(nibName: "PriceCell", bundle: nil)
        priceCollectionView.register(priceCellNib, forCellWithReuseIdentifier: "PriceCell")
    }
    
    @IBAction func onTapLogout(_ sender: UIBarButtonItem) {
        Defaults.isUserLogged = false
        guard let window = self.view.window else { return }
        Route().toLoginController(window: window)
    }
    
    private func setupNavigation(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }

}

extension PricingVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, PriceCellDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return prices.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PriceCell", for: indexPath) as! PriceCell
        
        let price = prices[indexPath.row]
        cell.priceImageView.image = price.image
        cell.priceTitleLabel.text = price.title
        cell.priceSubTitleLabel.text = price.subTitle
        cell.priceDescriptionLabel1.text = price.desc1
        cell.priceDescriptionLabel2.text = price.desc2
        cell.priceDescriptionLabel3.text = price.desc3
        cell.priceDescriptionLabel4.text = price.desc4
        cell.priceBackgroundView.backgroundColor = price.color
        cell.priceBackgroundView.layer.cornerRadius = 8
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: collectionView.frame.height - 20)
    }
    
    func payNowBtnCLicked(_ cell: UICollectionViewCell) {
        let indexPath = priceCollectionView.indexPath(for: cell)!
        let ballon = prices[indexPath.row]
        gotoPayPal(WithName: ballon.title, withPrice: ballon.price)
    }
    
    
    private func gotoPayPal(WithName name: String, withPrice price: String) {
        //These are the items choosen by user, for example
        let item = PayPalItem(name: name, withQuantity: 1, withPrice: NSDecimalNumber(string: price), withCurrency: "USD", withSku: "Hip-0037")
        let subtotal = PayPalItem.totalPrice(forItems: [item]) //This is the total price of all the items
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "0")
        let tax = NSDecimalNumber(string: "0")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        let total = subtotal.adding(shipping).adding(tax) //This is the total price including shipping and tax
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: Defaults.user.name, intent: .sale)
        payment.items = [item]
        payment.paymentDetails = paymentDetails
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn’t be processable, and you’d want
            // to handle that here.
            print("Payment not processalbe: (payment)")
        }

    }

}


extension PricingVC : PayPalPaymentDelegate {
    
    // PayPalPaymentDelegate
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:nn(completedPayment.confirmation)nnSend this to your server for confirmation and fulfillment.")
        })
    }

}
